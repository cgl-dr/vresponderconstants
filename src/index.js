import * as features from './features'
import * as forms from './forms'
import * as headers from './headers'
import users from './users'

import actions from './Actions'
import enums from './Enums'
import keys from './Keys'
import messages from './Messages'

const Constants = {
  Action: { ...actions },
  Enum: { ...enums },
  Feature: { ...features },
  Form: { ...forms },
  Header: { ...headers },
  Key: { ...keys },
  Message: { ...messages },
  User: { ...users }
}

export default Constants
