import * as callBackStatus from './callBackStatus'
import * as callState from './callState'
import * as callType from './callType'
import * as mobileOS from './mobileOS'
import * as notificationType from './notificationType'
import * as uploadState from './uploadState'

export default {
  CallBackStatus: { ...callBackStatus },
  CallState: { ...callState },
  CallType: { ...callType },
  MobileOS: { ...mobileOS },
  NotificationType: { ...notificationType },
  UploadState: { ...uploadState }
}
