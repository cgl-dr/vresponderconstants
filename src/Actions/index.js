import * as call from './call'
import * as callBack from './callBack'
import * as error from './error'
import * as jurisdiction from './jurisdiction'
import * as notification from './notification'
import * as product from './product'
import * as runtime from './runtime'
import * as table from './table'
import * as tag from './tag'
import * as team from './team'
import * as topic from './topic'
import * as ui from './ui'
import * as upload from './upload'
import * as user from './user'

export default {
  Call: { ...call },
  CallBack: { ...callBack },
  Error: { ...error },
  Jurisdiction: { ...jurisdiction },
  Notification: { ...notification },
  Product: { ...product },
  Runtime: { ...runtime },
  Table: { ...table },
  Tag: { ...tag },
  Team: { ...team },
  Topic: { ...topic },
  UI: { ...ui },
  Upload: { ...upload },
  User: { ...user }
}
