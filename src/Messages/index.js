import * as call from './call'
import * as callQueue from './callQueue'

export default {
  Call: { ...call },
  CallQueue: { ...callQueue },
  NOTIFICATION: 'WS_NOTIFICATION',
  CHAT_MESSAGE: 'WS_CHAT_MESSAGE'
}
