import resolve from 'rollup-plugin-node-resolve'

export default {
  input: 'src/index.js',
  output: {
    file: 'lib/vresponder-constants.js',
    format: 'cjs'
  },
  plugins: [
    resolve()
  ]
}
